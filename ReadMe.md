# Alien Planet
© 1990 by Tony Kingsmill

Genre: Science Fiction

YUID:  3ql1ecykplizyot 

IFDB: https://ifdb.org/viewgame?id=3ql1ecykplizyot 

Play ZX Spectrum original online: -

## Port Info

This is an act of love, a try to port old ZX IF games to ngPAWS to be able to play them on a browser.
Keep in mind this port is still in alpha (or pre-alpha state) there will be errors and issues. Please, test and report or help with the code :)

Tools: [UnQuill](http://www.seasip.info/Unix/UnQuill/), [The Inker](https://github.com/8bat/the_inker/), [ngPAWS](https://github.com/Utodev/ngPAWS/)
- N Game fully tested?
- N Missing images?
- - Errors rendering images?
- Y Modern code for ngPAWS?
- 1 File to edit: txp (1) or sce (2)

## Testing ngPAWS Port

Test alpha port: https://interactivefiction.gitlab.io/Alien_Planet-1990-Tony_Kingsmill/
and report issues: https://gitlab.com/interactivefiction/Alien_Planet-1990-Tony_Kingsmill/-/issues/
