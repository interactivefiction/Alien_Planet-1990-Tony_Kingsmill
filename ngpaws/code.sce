






;#define const aLight       = 0
;#define const aWearable    = 1
;#define const aContainer   = 2
;#define const aNPC         = 3
;#define const aConcealed   = 4
;#define const aEdible      = 5
;#define const aDrinkable   = 6
;#define const aEnterable   = 7
;#define const aFemale      = 8
;#define const aLockable    = 9
;#define const aLocked      = 10
;#define const aMale        = 11
;#define const aNeuter      = 12
;#define const aOpenable    = 13
;#define const aOpen        = 14
;#define const aPluralName  = 15
;#define const aTransparent = 16
;#define const aScenery     = 17
;#define const aSupporter   = 18
;#define const aSwitchable  = 19
;#define const aOn          = 20
;#define const aStatic      = 21


;#define const object_0_id = 0
;#define const object_1_gas = 1
;#define const object_2_hove = 2
;#define const object_3_can = 3
;#define const object_4_can = 4
;#define const object_5_hove = 5
;#define const object_6_shoe = 6
;#define const object_7_pot = 7
;#define const object_8_gold = 8
;#define const object_9_key = 9
;#define const object_10_fuel = 10
;#define const object_11_oxyg = 11
;#define const object_12_grav = 12


;#define const room_0 = 0
;#define const room_1 = 1
;#define const room_2 = 2
;#define const room_3 = 3
;#define const room_4 = 4
;#define const room_5 = 5
;#define const room_6 = 6
;#define const room_7 = 7
;#define const room_8 = 8
;#define const room_9 = 9
;#define const room_10 = 10
;#define const room_11 = 11
;#define const room_12 = 12
;#define const room_13 = 13
;#define const room_14 = 14
;#define const room_15 = 15
;#define const room_16 = 16
;#define const room_17 = 17
;#define const room_18 = 18
;#define const room_19 = 19
;#define const room_20 = 20
;#define const room_21 = 21
;#define const room_22 = 22
;#define const room_23 = 23
;#define const room_24 = 24
;#define const room_25 = 25
;#define const room_26 = 26
;#define const room_27 = 27
;#define const room_28 = 28
;#define const room_29 = 29
;#define const room_30 = 30
;#define const room_31 = 31
;#define const room_32 = 32
;#define const room_33 = 33
;#define const room_34 = 34
;#define const room_35 = 35
;#define const room_36 = 36
;#define const room_37 = 37
;#define const room_38 = 38
;#define const room_39 = 39
;#define const room_40 = 40
;#define const room_41 = 41
;#define const room_42 = 42
;#define const room_43 = 43
;#define const room_44 = 44
;#define const room_45 = 45
;#define const room_46 = 46

;#define const room_noncreated = 252 ; destroying an object moves it to this room
;#define const room_worn = 253 ; wearing an object moves it to this room
;#define const room_carried = 254 ; carrying an object moves it to this room
;#define const room_here = 255 ; not used by any Quill condacts










;#define const yesno_is_dark                       =   0
;#define const total_carried                       =   1
;#define const countdown_location_description      =   2
;#define const countdown_location_description_dark =   3
;#define const countdown_object_description_unlit  =   4
;#define const countdown_player_input_1            =   5
;#define const countdown_player_input_2            =   6
;#define const countdown_player_input_3            =   7
;#define const countdown_player_input_4            =   8
;#define const countdown_player_input_dark         =   9
;#define const countdown_player_input_unlit        =  10
;#define const game_flag_11                        = 111 ; ngPAWS assigns a special meaning to flag 11
;#define const game_flag_12                        = 112 ; ngPAWS assigns a special meaning to flag 12
;#define const game_flag_13                        = 113 ; ngPAWS assigns a special meaning to flag 13
;#define const game_flag_14                        = 114 ; ngPAWS assigns a special meaning to flag 14
;#define const game_flag_15                        = 115 ; ngPAWS assigns a special meaning to flag 15
;#define const game_flag_16                        = 116 ; ngPAWS assigns a special meaning to flag 16
;#define const game_flag_17                        =  17
;#define const game_flag_18                        =  18
;#define const game_flag_19                        =  19
;#define const game_flag_20                        =  20
;#define const game_flag_21                        =  21
;#define const game_flag_22                        =  22
;#define const game_flag_23                        =  23
;#define const game_flag_24                        =  24
;#define const game_flag_25                        =  25
;#define const game_flag_26                        =  26
;#define const game_flag_27                        =  27
;#define const pause_parameter                     =  28
;#define const bitset_graphics_status              =  29
;#define const total_player_score                  =  30
;#define const total_turns_lower                   =  31
;#define const total_turns_higher                  =  32
;#define const word_verb                           =  33
;#define const word_noun                           =  34
;#define const room_number                         =  38 ; (35 in The Quill, but 38 in ngPAWS)




;#define const pause_sound_effect_tone_increasing = 1 ; increasing tone (value is the duration)
;#define const pause_sound_effect_telephone       = 2 ; ringing telephone (value is the number of rings)
;#define const pause_sound_effect_tone_decreasing = 3 ; decreasing tone (opposite of effect 1)
;#define const pause_sound_effect_white_noise     = 5 ; white noise (resembles an audience clapping, value is the number of repetitions)
;#define const pause_sound_effect_machine_gun     = 6 ; machine gun noise (value is the duration)

;#define const pause_flash = 4 ; flash the screen and the border (value is the number of flashes)
;#define const pause_box_wipe = 9 ; a series of coloured boxes expands out of the centre of the screen at speed

;#define const pause_font_default = 7 ; switch to the default font
;#define const pause_font_alternate = 8 ; switch to the alternative font.

;#define const pause_change_youcansee = 10 ; Change the "You can also see" message to the message number passed to PAUSE

;#define const pause_restart = 12 ; Restart the game without warning.
;#define const pause_reboot = 13 ; Reboot the Spectrum without warning
;#define const pause_ability = 11 ; Set the maximum number of objects carried at once to the value passed to PAUSE
;#define const pause_ability_plus = 14 ; Increase number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_ability_minus = 15 ; Decrease number of objects that can be carried at once by the amount passed to PAUSE
;#define const pause_toggle_graphics = 19 ; switch graphics off (for PAUSE 255) or on (for any other value)
;#define const pause_ram_save_load = 21 ; RAMload (for PAUSE 50) or RAMsave (for any other value)
;#define const pause_set_identity_byte = 22 ; Set the identity in saved games to the value passed to PAUSE

;#define const pause_click_effect_1 = 16
;#define const pause_click_effect_2 = 17
;#define const pause_click_effect_3 = 18









/CTL






/VOC






north     1    verb
nort      1    verb
n         1    verb
south     2    verb
sout      2    verb
s         2    verb
east      3    verb
e         3    verb
west      4    verb
w         4    verb
northeast 5    verb
ne        5    verb
northwest 6    verb
nw        6    verb
se        7    verb
southeast 7    verb
sw        8    verb
southwest 8    verb
clim      9    verb
climb     9    verb
up        9    verb
u         9    verb
d        10    verb
desc     10    verb
down     10    verb






scor     20    verb
score    20    verb
help     21    verb
hint     21    verb
stuc     21    verb
stuck    21    verb
fill     22    verb
can      23    noun
card     24    noun
id       24    noun
idca     24    noun
idcard   24    noun
pour     25    verb
gas      26    noun
mask     26    noun
ente     27    verb
enter    27    verb
inse     27    verb
insert   27    verb
hove     28    noun
hover    28    noun
hovercraft 28    noun
shoe     29    noun
pot      30    noun
man      31    noun
talk     32    verb
give     33    verb
gold     34    noun
exam     35    verb
ex       35    verb
read     35    verb
x        35    verb
key      36    noun
barc     37    noun
barcreature 37    noun
oxte     38    noun
oxtec    38    noun
nire     39    noun
nirek    39    noun
noti     40    noun
notice   40    noun
safe     41    noun
say      42    verb
ryzt     43    noun
ryzton   43    noun
cann     44    noun
cannister 44    noun
fuel     44    noun
conv     45    noun
converter 45    noun
oxyg     45    noun
oxygen   45    noun
brea     46    verb
break    46    verb
smas     46    verb
smash    46    verb
trap     47    noun
cont     48    noun
controller 48    noun
grav     48    noun
gravity  48    noun
star     49    verb
turn     49    verb






rub      55    verb
get     100    verb
take    100    verb
drop    101    verb
remo    102    verb
remove  102    verb
wear    103    verb
i       104    noun
invent  104    verb
inve    104    noun
inventory 104    verb
l       105    verb
look    105    verb
r       105    verb
rede    105    verb
redesc  105    verb
redescribe 105    verb
text    105    verb
q       106    verb
quit    106    verb
stop    106    verb
save    107    verb
load    108    verb
wait    200    verb
atta    201    verb
attack  201    verb
figh    201    verb
fight   201    verb
kill    201    verb
kiss    202    verb
love    202    verb
bast    203    verb
bastard 203    verb
bitc    203    verb
bitch   203    verb
bloo    203    verb
crap    203    verb
cunt    203    verb
fuck    203    verb
shit    203    verb
bazn    235    noun
bazno   235    noun
changecss 236  verb
saved    237   noun
games    238   noun




AND          2    conjunction
THEN         2    conjunction






/STX





/0
It is dark. You can't see.<br>
/1
\nYou can also see:—<br>
/2
\nWhat should you do now?<br>
/3
\nWhat next?<br>
/4
\nWhat now?<br>
/5
\nWhat should you do now?<br>
/6
Don't understand —\ntry something else.<br>
/7
You can't go in that direction.<br>
/8
You can't.<br>
/9
You have with you:-
/10
(which you are wearing)
/11
Nothing at all.<br>
/12
Do you really want to quit now?<br>
/13
End of game — Play again?<br>
/14
Good bye…<br>
/15
OK.<br>
/16
{CLASS|center|Press any key to continue}<br>
/17
You have taken 
/18
 turn
/19
s
/20
.<br>
/21
You have completed 
/22
% of the game.<br>
/23
You're not wearing it.<br>
/24
You can't — your hands are full.<br>
/25
You already have it.<br>
/26
It's not here.<br>
/27
You can't carry any more.<br>
/28
You don't have it.<br>
/29
You're already wearing it.<br>
/30
Y<br>
/31
N<br>
/32
More…<br>
/33
><br>
/34
<br>
/35
Time passes…<br>
/36

/37

/38

/39

/40
You can't.<br>
/41
You can't.<br>
/42
You can't — your hands are full.<br>
/43
You can't carry any more.<br>
/44
You put {OREF} into<br>
/45
{OREF} is not into<br>
/46
<br>
/47
<br>
/48
<br>
/49
You don't have it.<br>
/50
You can't.<br>
/51
.<br>
/52
That is not into<br>
/53
nothing at all<br>
/54
File not found.<br>
/55
File corrupt.<br>
/56
I/O error. File not saved.<br>
/57
Directory full.<br>
/58
Please enter savegame name you used when saving the game status.
/59
Invalid savegame name. Please check the name you entered is correct, and make sure you are trying to load the game from the same browser you saved it.<br>
/60
Please enter savegame name. Remember to note down the name you choose (Or type 'SAVED GAMES' to retrieve saved game names), as it will be requested in order to restore the game status.
/61
<br>
/62
Sorry? Please try other words.<br>
/63
Here<br>
/64
you can see<br>
/65
you can see<br>
/66
inside you see<br>
/67
on top you see<br>






/MTX


/1000
Exits: 
/1001
You can't see any exits

/1003
{ACTION|nort|north}
/1004
{ACTION|sout|south}
/1005
{ACTION|east|east}
/1006
{ACTION|west|west}
/1007
{ACTION|ne|ne}
/1008
{ACTION|nw|nw}
/1009
{ACTION|se|se}
/1010
{ACTION|sw|sw}
/1011
{ACTION|up|up}






/OTX
/0 ; sust(object_0_id//0)
An ID Card.
/1 ; sust(object_1_gas//1)
A Gas Mask.
/2 ; sust(object_2_hove//2)
A small Hovercraft.
/3 ; sust(object_3_can//3)
A Can.
/4 ; sust(object_4_can//4)
A Can of Fuel.
/5 ; sust(object_5_hove//5)
A small Hovercraft.
/6 ; sust(object_6_shoe//6)
A pair of Shoes.
/7 ; sust(object_7_pot//7)
A Pot.
/8 ; sust(object_8_gold//8)
Some Gold.
/9 ; sust(object_9_key//9)
A Key.
/10 ; sust(object_10_fuel//10)
A Fuel Cannister.
/11 ; sust(object_11_oxyg//11)
An Oxygen Converter.
/12 ; sust(object_12_grav//12)
A Gravity Controller.






/LTX
/0 ; sust(room_0//0)
You are by the wreckage of your spacecraft, now just a heap of metal. You are surrounded by a large open area, with exits going {ACTION|north|north}, {ACTION|northeast|northeast} and {ACTION|west|west}.
/1 ; sust(room_1//1)
You're standing by a large pool of fuel from your spacecraft. An exit leads {ACTION|south|south} to the wreckage of the craft.
/2 ; sust(room_2//2)
You are on a flat, plain dusty track. In the distance you can see high volcanoes stretching into the sky. Paths lead {ACTION|east|east} and {ACTION|southwest|southwest}.
/3 ; sust(room_3//3)
You are by a number of tall sandstone pillars, badly eroded by the weather. Exits go {ACTION|west|west}, {ACTION|southeast|southeast} and {ACTION|north|north}.
/4 ; sust(room_4//4)
A massive dust storm blocks a narrow pathway leading {ACTION|southeast|southeast} the only exit is {ACTION|northwest|northwest}.
/5 ; sust(room_5//5)
You are on a long path going through the barren landscape. The path leads {ACTION|north|north} and {ACTION|south|south}.
/6 ; sust(room_6//6)
You are at the end of a long path through the barren land. A path leads {ACTION|south|south}.
/7 ; sust(room_7//7)
You are on a large path covered with sand. Paths lead {ACTION|west|west} along a rocky path and {ACTION|east|east} to the wreckage of your spacecraft.
/8 ; sust(room_8//8)
You're on a rocky path leading through the empty landscape. Three exits can be seen, going {ACTION|west|west}, {ACTION|north|north} and {ACTION|east|east}.
/9 ; sust(room_9//9)
You are at the end of a short path. Exits lead {ACTION|east|east} to the rocky path.
/10 ; sust(room_10//10)
You are on a track through some strange red sand. A number of routes can be taken, one leading {ACTION|north|north} along a mountain path and others leading {ACTION|northeast|northeast} {ACTION|east|east} and {ACTION|south|south}.
/11 ; sust(room_11//11)
You are at the end of a short track of red sand. The only exit is {ACTION|west|west} to a junction.
/12 ; sust(room_12//12)
You are on a track blocked by a huge boulder. The only exit is southwest to the red sand track.
/13 ; sust(room_13//13)
You're on a mountain path, which is well worn by some sort of animal. You can see two exits, {ACTION|south|south} and {ACTION|north|north}.
/14 ; sust(room_14//14)
You are on the mountain path. The path takes a sharp bend. You can now see paths leading {ACTION|south|south} and {ACTION|east|east} to a volcano.
/15 ; sust(room_15//15)
You're on the edge of a massive volcano. Volcanic ash and steaming debris coats the side of the huge volcano. Paths lead {ACTION|east|east} and {ACTION|west|west} away from the volcano.
/16 ; sust(room_16//16)
You're at a junction on the edge of the crater of the volcano. Exits can be seen going {ACTION|west|west} and {ACTION|east|east} along the edge and {ACTION|north|north} to a table of rock jutting over the edge.
/17 ; sust(room_17//17)
You are at the end of a path leading around the crater of the volcano. The only path is {ACTION|west|west} to a junction.
/18 ; sust(room_18//18)
You are on a table of rock jutting out over the edge of the crater. The northern side of the crater can be clearly seen from here. The only path is {ACTION|south|south} to a junction.
/19 ; sust(room_19//19)
You are on the northern edge of the volcano crater. Paths lead {ACTION|north|north} and {ACTION|northwest|northwest}.
/20 ; sust(room_20//20)
You are standing to the north of the huge volcano. Exits lead {ACTION|west|west}, {ACTION|south|south} and {ACTION|southeast|southeast} to an area of extremely hot ground.
/21 ; sust(room_21//21)
You are at a junction near the large volcano. Paths go {ACTION|southeast|southeast} towards the volcano, {ACTION|east|east} and {ACTION|west|west} to gas-leaking area.
/22 ; sust(room_22//22)
You are in a dangerous gas-leaking area. Emerging through large cracks in the ground is a mixture of poisonous chemicals. Paths go {ACTION|west|west} and {ACTION|east|east} to a junction.
/23 ; sust(room_23//23)
You're by a large spring of warm water. Water sprays hundreds of feet above you. The only path is {ACTION|east|east} to a gas-leaking area.
/24 ; sust(room_24//24)
You are on very hot ground so hot it is painful to walk on. Deep cracks can be seen all around you. Paths lead {ACTION|northeast|northeast} and {ACTION|northwest|northwest}.
/25 ; sust(room_25//25)
You are on the edge of a large area of cracked ground. Exits can be seen going {ACTION|southwest|southwest} and {ACTION|east|east} to a village.
/26 ; sust(room_26//26)
Standing on the edge of the village, you can see a number of small houses in the distance. Exits lead {ACTION|northeast|northeast} and {ACTION|west|west}.
/27 ; sust(room_27//27)
You are in the centre of the village. Paths lead {ACTION|southwest|southwest} out of the village, {ACTION|east|east} into a hut and {ACTION|north|north} through the village.
/28 ; sust(room_28//28)
You are in a small, dimly lit hut. The hut consists of just one room, with a fireplace in the far corner. Set in front of the fireplace are two chairs, one is occupied by an old man, the other empty. The only exit is {ACTION|west|west} out of the hut.
/29 ; sust(room_29//29)
You are in the north of the village, marked by a few small huts. The only exit is {ACTION|south|south} to the centre of the village.
/30 ; sust(room_30//30)
You are at the entrance to a large alien city. The city looks very advanced, with tall strange buildings. Next to the main entrance is a signpost written in about twenty different languages, one of which is Engish, reading WELCOME. A path leads {ACTION|north|north} into the city.
/31 ; sust(room_31//31)
You are in a large, bright building which is the home of a man called Old Bazno. Old Bazno glances at you before giving a long sigh and going back to sleep. The only exit is {ACTION|east|east} out of the building.
/32 ; sust(room_32//32)
You are outside a large, smart looking building. The door has been left wide open. Exits lead {ACTION|east|east} and {ACTION|west|west} into the building.
/33 ; sust(room_33//33)
You are at a large junction inside the main entrance to the city. Paths go {ACTION|north|north} into the Zapray Inn, {ACTION|south|south} out of the city, {ACTION|east|east} along a road and {ACTION|west|west} to a large building.
/34 ; sust(room_34//34)
You are in the Zapray Inn. Smoke fills the air from the illegal smoking of various narcotics. Strange, alien creatures occupy most of the inn, you being the only human in sight. You can see a BARCREATURE who looks quite friendly serving behind the bar. You also see an OXTEC alien and a NIREK. The exit out of the inn is {ACTION|south|south}.
/35 ; sust(room_35//35)
You are on a road leading through the alien city. Exits lead {ACTION|north|north} to the industrial area and {ACTION|west|west} to a junction.
/36 ; sust(room_36//36)
You are in the industrial area. Tall buildings surround you, producing unknown chemicals and machinery. Exits lead {ACTION|east|east} to an old building, {ACTION|north|north} and {ACTION|south|south}.
/37 ; sust(room_37//37)
You are in the industrial area. Two roads can be seen leading {ACTION|west|west} and {ACTION|south|south}.
/38 ; sust(room_38//38)
You're inside an old building, lit by a few dim bulbs above. Debris lies all around, the building appears to have seen better days. Exits lead {ACTION|west|west} out of the building and {ACTION|north|north} further into the building.
/39 ; sust(room_39//39)
You are next to a primitive looking spacecraft. The craft appears to be badly damaged and not used for quite some time. Exits lead {ACTION|north|north} into the space- craft and {ACTION|south|south}.
/40 ; sust(room_40//40)
Tou are inside the spacecraft. The controls look a little dated compared to your previous space- craft that crashed, nevertheless it would probably still work ok, prividing it has a new oxygen converter, gravity controller and a new fuel cannister. You also notice a hole for a key. The only exit is {ACTION|south|south} out of the spacecraft.
/41 ; sust(room_41//41)
You are on a road around the industrial area. Exits lead {ACTION|east|east} through the industrial area and {ACTION|north|north} to a junction.
/42 ; sust(room_42//42)
You're at a junction in the north of the city. Paths can be seen going {ACTION|west|west} to a hanger, {ACTION|south|south} to the industrial area and {ACTION|east|east} to LKM Electronics.
/43 ; sust(room_43//43)
You are outside a large old hanger, now totally disused, a reminder of when spacecraft could fly freely in and out of the planet. Paths lead {ACTION|west|west} into the hanger and {ACTION|east|east} to a junction.
/44 ; sust(room_44//44)
You're inside the large old hanger. You can see a dirty notice on one of the walls. In the middle of the floor is a safe. An exit leads {ACTION|east|east} out of the hanger.
/45 ; sust(room_45//45)
You are inside the building of LKM Electronics. No one can be seen except two robot guards which havn't seen you yet. You can see a trapdoor in the ground and an exit {ACTION|west|west} out of the building.
/46 ; sust(room_46//46)
You are in LKM's store room. Crates and boxes fill most of the room. An exit leads {ACTION|up|up} to the main part of the building.






/CON
/0 ; sust(room_0//0)
n 1 ; sust(room_1/1)
ne 2 ; sust(room_2/2)
w 7 ; sust(room_7/7)

/1 ; sust(room_1//1)
s 0 ; sust(room_0/0)

/2 ; sust(room_2//2)
e 3 ; sust(room_3/3)
sw 0 ; sust(room_0/0)

/3 ; sust(room_3//3)
n 5 ; sust(room_5/5)
se 4 ; sust(room_4/4)
w 2 ; sust(room_2/2)

/4 ; sust(room_4//4)
nw 3 ; sust(room_3/3)

/5 ; sust(room_5//5)
n 6 ; sust(room_6/6)
s 3 ; sust(room_3/3)

/6 ; sust(room_6//6)
s 5 ; sust(room_5/5)

/7 ; sust(room_7//7)
e 0 ; sust(room_0/0)
w 8 ; sust(room_8/8)

/8 ; sust(room_8//8)
e 7 ; sust(room_7/7)
n 10 ; sust(room_10/10)
w 9 ; sust(room_9/9)

/9 ; sust(room_9//9)
e 8 ; sust(room_8/8)

/10 ; sust(room_10//10)
e 11 ; sust(room_11/11)
n 13 ; sust(room_13/13)
ne 12 ; sust(room_12/12)
s 8 ; sust(room_8/8)

/11 ; sust(room_11//11)
w 10 ; sust(room_10/10)

/12 ; sust(room_12//12)
sw 10 ; sust(room_10/10)

/13 ; sust(room_13//13)
n 14 ; sust(room_14/14)
s 10 ; sust(room_10/10)

/14 ; sust(room_14//14)
e 15 ; sust(room_15/15)
s 13 ; sust(room_13/13)

/15 ; sust(room_15//15)
e 16 ; sust(room_16/16)
w 14 ; sust(room_14/14)

/16 ; sust(room_16//16)
e 17 ; sust(room_17/17)
n 18 ; sust(room_18/18)
w 15 ; sust(room_15/15)

/17 ; sust(room_17//17)
w 16 ; sust(room_16/16)

/18 ; sust(room_18//18)
s 16 ; sust(room_16/16)

/19 ; sust(room_19//19)
n 20 ; sust(room_20/20)
nw 21 ; sust(room_21/21)

/20 ; sust(room_20//20)
s 19 ; sust(room_19/19)
se 24 ; sust(room_24/24)
w 21 ; sust(room_21/21)

/21 ; sust(room_21//21)
e 20 ; sust(room_20/20)
se 19 ; sust(room_19/19)
w 22 ; sust(room_22/22)

/22 ; sust(room_22//22)
e 21 ; sust(room_21/21)
w 23 ; sust(room_23/23)

/23 ; sust(room_23//23)
e 22 ; sust(room_22/22)

/24 ; sust(room_24//24)
ne 25 ; sust(room_25/25)
nw 20 ; sust(room_20/20)

/25 ; sust(room_25//25)
e 26 ; sust(room_26/26)
sw 24 ; sust(room_24/24)

/26 ; sust(room_26//26)
ne 27 ; sust(room_27/27)
w 25 ; sust(room_25/25)

/27 ; sust(room_27//27)
e 28 ; sust(room_28/28)
n 29 ; sust(room_29/29)
sw 26 ; sust(room_26/26)

/28 ; sust(room_28//28)
w 27 ; sust(room_27/27)

/29 ; sust(room_29//29)
s 27 ; sust(room_27/27)

/30 ; sust(room_30//30)
n 33 ; sust(room_33/33)

/31 ; sust(room_31//31)
e 32 ; sust(room_32/32)

/32 ; sust(room_32//32)
e 33 ; sust(room_33/33)
w 31 ; sust(room_31/31)

/33 ; sust(room_33//33)
e 35 ; sust(room_35/35)
n 34 ; sust(room_34/34)
s 30 ; sust(room_30/30)
w 32 ; sust(room_32/32)

/34 ; sust(room_34//34)
s 33 ; sust(room_33/33)

/35 ; sust(room_35//35)
n 36 ; sust(room_36/36)
w 33 ; sust(room_33/33)

/36 ; sust(room_36//36)
e 38 ; sust(room_38/38)
n 37 ; sust(room_37/37)
s 35 ; sust(room_35/35)

/37 ; sust(room_37//37)
s 36 ; sust(room_36/36)
w 41 ; sust(room_41/41)

/38 ; sust(room_38//38)
n 39 ; sust(room_39/39)
w 36 ; sust(room_36/36)

/39 ; sust(room_39//39)
n 40 ; sust(room_40/40)
s 38 ; sust(room_38/38)

/40 ; sust(room_40//40)
s 39 ; sust(room_39/39)

/41 ; sust(room_41//41)
e 37 ; sust(room_37/37)
n 42 ; sust(room_42/42)

/42 ; sust(room_42//42)
e 45 ; sust(room_45/45)
s 41 ; sust(room_41/41)
w 43 ; sust(room_43/43)

/43 ; sust(room_43//43)
e 42 ; sust(room_42/42)
w 44 ; sust(room_44/44)

/44 ; sust(room_44//44)
e 43 ; sust(room_43/43)

/45 ; sust(room_45//45)
w 42 ; sust(room_42/42)

/46 ; sust(room_46//46)
u 45 ; sust(room_45/45)







/OBJ

/0             4      1    _    _       10000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_0_id//0) ; sust(room_4/4) ; sust(ght/ATTR)
/1            5      1    _    _       01000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_1_gas//1) ; sust(room_5/5) ; sust(arable/ATTR)
/2           6      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_2_hove//2) ; sust(room_6/6) ; sust(ATTR/ATTR)
/3            9      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_3_can//3) ; sust(room_9/9) ; sust(ATTR/ATTR)
/4   252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_4_can//4) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/5  252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_5_hove//5) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/6          21      1    _    _       01000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_6_shoe//6) ; sust(room_21/21) ; sust(arable/ATTR)
/7           29      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_7_pot//7) ; sust(room_29/29) ; sust(ATTR/ATTR)
/8          30      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_8_gold//8) ; sust(room_30/30) ; sust(ATTR/ATTR)
/9   252      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_9_key//9) ; sust(room_noncreated/252) ; sust(ATTR/ATTR)
/10         43      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_10_fuel//10) ; sust(room_43/43) ; sust(ATTR/ATTR)
/11         45      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_11_oxyg//11) ; sust(room_45/45) ; sust(ATTR/ATTR)
/12         46      1    _    _       00000000000000000000000000000000 00000000000000000000000000000000 ; sust(object_12_grav//12) ; sust(room_46/46) ; sust(ATTR/ATTR)






/PRO 0








_ _
 hook    "RESPONSE_START"


_ _
 hook    "RESPONSE_USER"

d _
 at      45 ; sust(room_45/45)
 eq      22 1 ; sust(game_flag_22/22)
 goto    46 ; sust(room_46/46)
 cls
 desc

scor _
 score
 turns

help _
 writeln "The commands are:- N, E, S, W, NE, NW, SE, SW, INVENTORY(I), LOOK(L), HELP, HINT, SCORE, QUIT, SAVE, LOAD, UP(U), DOWN(D), REMOVE, WEAR, GET, DROP, TAKE, FILL, POUR, ENTER, READ, TALK, EXAMINE, GIVE, RUB, SAY, SMASH, TURN, START, BREAK."
 done

fill can
 at      1 ; sust(room_1/1)
 carried 3 ; sust(object_3_can/3)
 plus    30 5 ; sust(total_player_score/30)
 destroy 3 ; sust(object_3_can/3)
 create  4 ; sust(object_4_can/4)
 get     4 ; sust(object_4_can/4)
 ok

fill pot
 at      23 ; sust(room_23/23)
 carried 7 ; sust(object_7_pot/7)
 eq      26 0 ; sust(game_flag_26/26)
 writeln "You have filled the pot with spring water."
 let     26 1 ; sust(game_flag_26/26)
 done

pour fuel
 carried 4 ; sust(object_4_can/4)
 LET 43 23      ; Pour fuel -> pour can ; sust(_voc_CAN/23)

pour can
 carried 2 ; sust(object_2_hove/2)
 carried 4 ; sust(object_4_can/4)
 plus    30 10 ; sust(total_player_score/30)
 destroy 2 ; sust(object_2_hove/2)
 destroy 4 ; sust(object_4_can/4)
 create  5 ; sust(object_5_hove/5)
 get     5 ; sust(object_5_hove/5)
 ok

ente id
 eq      25 0 ; sust(game_flag_25/25)
 at      18 ; sust(room_18/18)
 carried 0 ; sust(object_0_id/0)
 carried 5 ; sust(object_5_hove/5)
 let     25 1 ; sust(game_flag_25/25)
 plus    30 5 ; sust(total_player_score/30)
 writeln "You insert the ID card and the hovercraft starts. It takes you north across the volcano."
 anykey
 goto    19 ; sust(room_19/19)
 cls
 desc

ente id
 eq      25 1 ; sust(game_flag_25/25)
 at      18 ; sust(room_18/18)
 carried 0 ; sust(object_0_id/0)
 carried 5 ; sust(object_5_hove/5)
 writeln "You insert the ID card and the hovercraft starts. It takes you north across the volcano."
 anykey
 cls
 goto    19 ; sust(room_19/19)
 desc

ente id
 at      19 ; sust(room_19/19)
 carried 0 ; sust(object_0_id/0)
 carried 5 ; sust(object_5_hove/5)
 writeln "You insert the card into the hovercraft and it starts. The hovercraft takes you south."
 anykey
 cls
 goto    18 ; sust(room_18/18)
 desc

ente key
 carried 9 ; sust(object_9_key/9)
 at      40 ; sust(room_40/40)
 plus    20 1 ; sust(game_flag_20/20)
 writeln "You have inserted the key ready to start."
 destroy 9 ; sust(object_9_key/9)
 done

talk man
 at      28 ; sust(room_28/28)
 writeln "The wise man says to you &#34;Ah! young adventurer, you must help those in this village with a dreadful disease. You must collect the spring water which can be found west from the village. Complete this task and you will be fully rewarded.&#34;"
 done

talk barc
 at      34 ; sust(room_34/34)
 writeln "The bar-creature replies, &#34;Go to Bazno for some help - he came from Earth.&#34;"
 done

talk oxte
 at      34 ; sust(room_34/34)
 writeln "The Oxtec says, &#34;LKM have what you need, but watch out - it's dangerous.&#34;"
 done

talk nire
 at      34 ; sust(room_34/34)
 writeln "The Nirek says, &#34;I don't like you, so watch it or there will be trouble!&#34;"
 done

talk bazn
 at      31 ; sust(room_31/31)
 writeln "Bazno lazily looks at you, saying,&#34;I only talk for gold…&#34;"
 done

give pot
 at      28 ; sust(room_28/28)
 carried 7 ; sust(object_7_pot/7)
 eq      26 1 ; sust(game_flag_26/26)
 plus    30 10 ; sust(total_player_score/30)
 writeln "The wise man is very pleased with you. He says, &#34;Thank you very much young adventurer! You have saved many lives! I will now use the best of my magic to teleport you to a place on the planet where you will be able to find a way to return home."
 anykey
 cls
 goto    30 ; sust(room_30/30)
 desc

give gold
 at      31 ; sust(room_31/31)
 carried 8 ; sust(object_8_gold/8)
 destroy 8 ; sust(object_8_gold/8)
 writeln "Bazno says, &#34;This planet is ruled by a strict communist type government. No spaceship is allowed on or off of the planet. I would say your only chance is to repair an old craft somewhere in the industrial area without anyone finding out.\n\nI crashed here many years ago due to a strong gravitational pull. The planet is unknown to Earth, anyway I like it here… &#34;Bazno goes to sleep."
 plus    30 5 ; sust(total_player_score/30)
 done

x can
 present 3 ; sust(object_3_can/3)
 writeln "CAN: It's just a normal looking empty can, big enough to hold about four litres and painted red."
 done

x can
 present 4 ; sust(object_4_can/4)
 writeln "CAN OF FUEL: A four litre can painted red and full of fuel."
 done

x id
 present 0 ; sust(object_0_id/0)
 writeln "ID CARD: A thin piece of plastic with some sort of code on it."
 done

x gas
 present 1 ; sust(object_1_gas/1)
 writeln "GAS MASK: A normal looking gas mask that can be worn in dangerous areas."
 done

x hove
 present 2 ; sust(object_2_hove/2)
 writeln "HOVERCRAFT: A small machine which you can sit in to be transported from one place to another. It has no fuel inside. There is a small gap to insert an identity card to start it."
 done

x hove
 present 5 ; sust(object_5_hove/5)
 writeln "HOVERCRAFT: A small machine for moving from one place to another It is full of fuel. There is a gap for inserting an identity card to start it."
 done

x shoe
 present 6 ; sust(object_6_shoe/6)
 writeln "SHOES: A bright blue pair of shoes with a very thick sole."
 done

x pot
 present 7 ; sust(object_7_pot/7)
 eq      26 0 ; sust(game_flag_26/26)
 writeln "POT: A large piece of hand-made pottery capable of holding liquid."
 done

x pot
 carried 7 ; sust(object_7_pot/7)
 eq      26 1 ; sust(game_flag_26/26)
 writeln "POT: A pot full of spring water."
 done

x gold
 present 8 ; sust(object_8_gold/8)
 writeln "GOLD: Consists of several heavy pieces of gold, probably worth a lot of money."
 done

x key
 present 9 ; sust(object_9_key/9)
 writeln "KEY: A small silver key, used for starting some sort of machinery."
 done

x noti
 eq      23 0 ; sust(game_flag_23/23)
 at      44 ; sust(room_44/44)
 writeln "The notice is too dirty to read."
 done

x noti
 at      44 ; sust(room_44/44)
 eq      23 1 ; sust(game_flag_23/23)
 writeln "The message reads,&#34;CODE: RYZTON&#34;"
 done

x safe
 at      44 ; sust(room_44/44)
 eq      21 0 ; sust(game_flag_21/21)
 writeln "The safe is protected with a speech lock."
 done

x safe
 at      44 ; sust(room_44/44)
 eq      21 1 ; sust(game_flag_21/21)
 writeln "You notice a key inside it."
 create  9 ; sust(object_9_key/9)
 done

x fuel
 present 10 ; sust(object_10_fuel/10)
 writeln "FUEL CANNISTER: A large cannister full of fuel to drive a very large piece of machinery."
 done

x oxyg
 present 11 ; sust(object_11_oxyg/11)
 writeln "OXYGEN CONVERTER: A strange device used on board primitive spacecraft to convert carbon dioxide into oxygen for fresh air in space."
 done

x grav
 present 12 ; sust(object_12_grav/12)
 writeln "GRAVITY CONTROLLER: A heavy piece of old machinery used on board spacecraft to control the gravity in space."
 done

say ryzt
 at      44 ; sust(room_44/44)
 eq      21 0 ; sust(game_flag_21/21)
 plus    30 10 ; sust(total_player_score/30)
 let     21 1 ; sust(game_flag_21/21)
 writeln "Well done! You have opened the safe!"
 done

smas trap
 at      45 ; sust(room_45/45)
 plus    30 10 ; sust(total_player_score/30)
 let     22 1 ; sust(game_flag_22/22)
 ok

turn key
 at      40 ; sust(room_40/40)
 eq      20 4 ; sust(game_flag_20/20)
 writeln "You turn the key and the engine shakes around and gradually stops! You try again and this time it comes to life, ready to take off. You increase the power and the spaceship lifts away from the planets surface, being watched by furious guards.\n  CONGRATULATIONS - YOU HAVE COMPLETED ALIEN PLANET!!     "
 score
 turns
 end

rub noti
 at      44 ; sust(room_44/44)
 eq      23 0 ; sust(game_flag_23/23)
 plus    30 5 ; sust(total_player_score/30)
 writeln "You rub the notice revealing a message."
 let     23 1 ; sust(game_flag_23/23)
 done

get can
 present 3 ; sust(object_3_can/3)
 get     3 ; sust(object_3_can/3)
 ok

get can
 present 4 ; sust(object_4_can/4)
 get     4 ; sust(object_4_can/4)
 ok

get id
 get     0 ; sust(object_0_id/0)
 ok

get gas
 get     1 ; sust(object_1_gas/1)
 ok

get hove
 present 2 ; sust(object_2_hove/2)
 get     2 ; sust(object_2_hove/2)
 ok

get hove
 present 5 ; sust(object_5_hove/5)
 get     5 ; sust(object_5_hove/5)
 ok

get shoe
 get     6 ; sust(object_6_shoe/6)
 ok

get pot
 get     7 ; sust(object_7_pot/7)
 ok

get gold
 get     8 ; sust(object_8_gold/8)
 ok

get key
 get     9 ; sust(object_9_key/9)
 ok

get fuel
 get     10 ; sust(object_10_fuel/10)
 ok

get oxyg
 get     11 ; sust(object_11_oxyg/11)
 ok

get grav
 get     12 ; sust(object_12_grav/12)
 ok

get i
 inven

drop can
 present 3 ; sust(object_3_can/3)
 drop    3 ; sust(object_3_can/3)
 ok

drop can
 present 4 ; sust(object_4_can/4)
 drop    4 ; sust(object_4_can/4)
 ok

drop id
 drop    0 ; sust(object_0_id/0)
 ok

drop gas
 drop    1 ; sust(object_1_gas/1)
 ok

drop hove
 present 2 ; sust(object_2_hove/2)
 drop    2 ; sust(object_2_hove/2)
 ok

drop hove
 present 5 ; sust(object_5_hove/5)
 drop    5 ; sust(object_5_hove/5)
 ok

drop shoe
 drop    6 ; sust(object_6_shoe/6)
 ok

drop pot
 drop    7 ; sust(object_7_pot/7)
 ok

drop gold
 drop    8 ; sust(object_8_gold/8)
 ok

drop key
 drop    9 ; sust(object_9_key/9)
 ok

drop fuel
 notat   40 ; sust(room_40/40)
 drop    10 ; sust(object_10_fuel/10)
 ok

drop fuel
 at      40 ; sust(room_40/40)
 carried 10 ; sust(object_10_fuel/10)
 plus    30 10 ; sust(total_player_score/30)
 destroy 10 ; sust(object_10_fuel/10)
 writeln "You drop the fuel cannister into the spacecraft. The spacecraft now has a supply of fuel."
 plus    20 1 ; sust(game_flag_20/20)
 done

drop oxyg
 notat   40 ; sust(room_40/40)
 drop    11 ; sust(object_11_oxyg/11)
 ok

drop oxyg
 at      40 ; sust(room_40/40)
 carried 11 ; sust(object_11_oxyg/11)
 plus    30 15 ; sust(total_player_score/30)
 destroy 11 ; sust(object_11_oxyg/11)
 writeln "You have dropped the Oxygen Converter into the spacecraft. The spacecraft now has a ready supply of oxygen in space."
 plus    20 1 ; sust(game_flag_20/20)
 done

drop grav
 notat   40 ; sust(room_40/40)
 drop    12 ; sust(object_12_grav/12)
 ok

drop grav
 at      40 ; sust(room_40/40)
 carried 12 ; sust(object_12_grav/12)
 plus    30 15 ; sust(total_player_score/30)
 destroy 12 ; sust(object_12_grav/12)
 writeln "You have droped the Gravity Controller into the spacecraft. The spacecraft will now be stable in space."
 plus    20 1 ; sust(game_flag_20/20)
 done

remo gas
 remove  1 ; sust(object_1_gas/1)
 ok

remo shoe
 remove  6 ; sust(object_6_shoe/6)
 ok

wear gas
 wear    1 ; sust(object_1_gas/1)
 ok

wear shoe
 wear    6 ; sust(object_6_shoe/6)
 ok

_ saved
 noun2  games
 LISTSAVEDGAMES
 DONE


_ _
 hook    "RESPONSE_DEFAULT_START"

_ i
 inven

r _
 desc

q _
 quit
 score
 turns
 end

save _
 save

load _
 load

wait _
 writeln "Time passes…"
 pause   50

figh _
 writeln "Isn't there enough violence on TV?"
 done

kiss _
 writeln "Now come on, sort yourself out. Is it really necessary at a time like this?"
 done

cunt _
 writeln "What did you b***d* say just then, you f***i** b*st*r* of a b*t**'s c**t!"
 done


changecss _
 CHANGECSS                  ; TODO: Pass parameters mobile, desktop, null, etc
 done


_ _
 hook    "RESPONSE_DEFAULT_END"







/PRO 1


_ _
 hook "PRO1"

_ _
 at     0
 bclear 12 5                      ; Set language to English

_ _
 islight
 listobj                        ; Lists present objects
 listnpc @38                    ; Lists present NPCs







/PRO 2


_ _
 eq      31 0 ; sust(total_turns_lower/31)
 eq      32 0 ; sust(total_turns_higher/32)
 ability 8 8


_ _
 hook    "PRO2"

_ _
 at      22 ; sust(room_22/22)
 notworn 1 ; sust(object_1_gas/1)
 writeln "Lethal gas is coming out of the ground here - and you are not wearing a gas mask! You begin to feel very sick before collapsing dead…"
 score
 turns
 end

_ _
 at      45 ; sust(room_45/45)
 chance  10
 writeln "Arrrhh!!!! The robots have seen you!!! They take you to the communist government where you will be subjected to prison for the rest of your life for snooping around in LKM's store.."
 score
 turns
 end

_ _
 at      24 ; sust(room_24/24)
 notworn 6 ; sust(object_6_shoe/6)
 writeln "The hot ground is far to hot for you to walk on and within minutes fries you! Your adventure is over…"
 score
 turns
 end
