//LIB IMPORTGAME

function importgame()
{
    var link = document.createElement( 'input' );
    link.setAttribute( 'type', 'file');
    link.setAttribute( 'id', 'inputjson');
    link.setAttribute( 'onchange', 'readJSONFile(this.files[0])');
    var event = document.createEvent( 'MouseEvents' );
    event.initMouseEvent( 'click', true, true, window, 1, 0, 0, 0, 0, false, false, false, false, 0, null);
    link.dispatchEvent( event );
}

async function readJSONFile(file) {
    let fileReader = new FileReader();
    fileReader.addEventListener("load", e => {
        try {
            var gameObject = JSON.parse(fileReader.result);
            restoreSaveGameObject(gameObject);
            if (getLang()=='EN') writelnText("Saved game imported. Loading"); else writelnText("Saved game importado. Cargando");
            ACCanykey();    //Only to make ACCdesc to work
    		ACCdesc();
    		focusInput();
        }
        catch (e) {
            if (getLang()=='EN') writelnText("Couldn't read json information. Wrong format?"); else writelnText("No se ha podido leer el json. ¿Error de formato?");
            console.log("Error loading saved game json", e); 
        }
    });

    
    fileReader.readAsText(file);
}
    

